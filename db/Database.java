package Latihan.db;

import java.sql.*;
import java.util.Scanner;


public class Database {
    static Connection con;
    static Statement stmt;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilih = 0;

        try {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (Exception e) {
                System.out.println(e);
            }
            con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "Pitak2003");

            stmt = con.createStatement();

            while (pilih != 99) {
                System.out.println("MENU");
                System.out.println("1. Show All Record");
                System.out.println("2. Insert Record");
                System.out.println("3. Update Record");
                System.out.println("4. Delete Record");
                System.out.println("99. Exit");
                System.out.println();
                System.out.print("Pilih menu : ");
                pilih = input.nextInt();

                switch (pilih) {
                    case 1:
                        System.out.println();
                        ResultSet rs = stmt.executeQuery("select * from emp");
                        while (rs.next())
                            System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                        System.out.println();
                        break;
                    case 2:
                        System.out.print("Masukkan nama : ");
                        String nama = input.next();
                        System.out.print("Masukkan umur : ");
                        int umur = input.nextInt();
                        String query1 = "insert into emp (name,age) values ('"+nama+"',"+umur+")";
                        stmt.executeUpdate(query1);
                        System.out.println("Insert Berhasil");
                        System.out.println();
                        break;
                    case 3:
                        System.out.print("Masukkan nama baru : ");
                        nama = input.next();
                        System.out.print("Masukkan id yang akan di update : ");
                        int id = input.nextInt();
                        String query2 = "update emp set name = '"+nama+"' where id in ("+id+")";
                        stmt.executeUpdate(query2);
                        System.out.println("Update Berhasil");
                        System.out.println();
                        break;
                    case 4:
                        System.out.print("Input id : ");
                        id = input.nextInt();
                        stmt.executeUpdate("delete from sonoo.emp where id = " + id);
                        System.out.println();
                    case 99:
                        break;
                    default:
                        System.out.println("tidak ada pilihan tersebut");
                }
            }
        } catch (SQLException excep){
            excep.printStackTrace();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
