package Latihan.prepstmt;

import java.sql.*;
import java.util.Scanner;

public class SqlPrepStmt {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilih = 0;

        try {
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
            } catch (Exception e) {
                System.out.println(e);
            }
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/sonoo", "root", "Pitak2003");

            Statement stmt = con.createStatement();

            while (pilih != 99) {
                System.out.println("MENU");
                System.out.println("1. Show All Record");
                System.out.println("2. Insert Record");
                System.out.println("3. Update Record");
                System.out.println("4. Delete Record");
                System.out.println("99. Exit");
                System.out.println();
                System.out.print("Pilih menu : ");
                pilih = input.nextInt();

                switch (pilih) {
                    case 1:
                        System.out.println();
                        System.out.println();
                        ResultSet rs = stmt.executeQuery("select * from emp");
                        while (rs.next())
                            System.out.println(rs.getInt(1) + "  " + rs.getString(2) + "  " + rs.getString(3));
                        System.out.println();
                        break;
                    case 2:
                        System.out.print("Masukkan nama : ");
                        String nama = input.next();
                        System.out.print("Masukkan umur : ");
                        int umur = input.nextInt();
                        PreparedStatement preparedStatement = con.prepareStatement("Insert into emp (name, age) values (?,?)");
                        preparedStatement.setString(1, nama);
                        preparedStatement.setInt(2, umur);

                        int i = preparedStatement.executeUpdate();
                        System.out.println(i+ " Insert Berhasil");
                        break;
                    case 3:
                        System.out.print("Masukkan nama baru : ");
                        nama = input.next();
                        System.out.print("Masukkan id yang akan di update : ");
                        int id = input.nextInt();
                        preparedStatement = con.prepareStatement("Update emp set name = (?) where id in (?)");
                        preparedStatement.setString(1, nama);
                        preparedStatement.setInt(2, id);

                        i = preparedStatement.executeUpdate();
                        System.out.println(i+ " Update Berhasil");
                        break;
                    case 4:
                        System.out.print("Input id : ");
                        id = input.nextInt();
                        preparedStatement = con.prepareStatement("delete from sonoo.emp where id = (?)");
                        preparedStatement.setInt(1, id);

                        i = preparedStatement.executeUpdate();
                        System.out.println(i + " Delete berhasil");
                        System.out.println();
                    case 99:
                        break;
                    default:
                        System.out.println("tidak ada pilihan tersebut");
                }
            }
        } catch (SQLException excep){
            excep.printStackTrace();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
